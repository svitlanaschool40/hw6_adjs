const URL = "https://api.ipify.org/?format=json";
const button = document.querySelector("button");

async function getIp(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}

async function getAdress(ip) {
  const response = await fetch(`http://ip-api.com/json/${ip}`);
  const data = await response.json();
  return data;
}

button.addEventListener("click", async () => {
  const ipAdress = await getIp(URL);
  const Adress = await getAdress(ipAdress.ip);
  let { countryCode, country, city, regionName, region } = Adress;
  const adress = document.createElement("p");
  adress.textContent = `${countryCode} - ${country} - ${regionName} (номер регіону ${region}) - ${city} `;

  button.after(adress);

  // .then((data) => getAdress(data.ip))
  // .then((data) => {
  //   const adress = document.createElement("p");
  //   let { countryCode, country, city, regionName, region } = data;
  //   adress.textContent = `${countryCode} - ${country} - ${regionName} (номер регіону ${region}) - ${city} `;

  //   button.after(adress);
  // });
});
